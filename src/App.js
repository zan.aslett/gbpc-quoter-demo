import './App.css';
import {useState} from "react";
const quoter = require('gbpc-quoter');

function App() {
    const REQUEST = {
        req: {
            parts: [
                {
                    "name": "part name",
                    "color": "Satin Black",
                    "partCount": 5,
                    "perPartSqrft": 4.5,
                    "requireZinc": false,
                    "requireClearCoat": false,
                    "id": "partId_1"
                },
                {
                    "name": "1Z7498D940FJ",
                    "color": "Burnt Orange",
                    "partCount": 13,
                    "perPartSqrft": 4.5,
                    "requireZinc": false,
                    "requireClearCoat": false,
                    "id": "partId_2"
                }
            ],
            arrivalDate: "2020-12-09",
            volumes: "10,100"
        }
    }
    const TARGET_DAYS = 5;
    const apiToken = 'YOUR-TOKEN-HERE';

    const [estimate, setEstimate] = useState();
    const [deliveryQuote, setDeliveryQuote] = useState();
    const [finalQuote, setFinalQuote] = useState();
    const [projectReceipt, setProjectReceipt] = useState();

    async function init() {
        await quoter.init(apiToken);
        console.log('Initialized!');
    }

    async function getEstimate() {
        if (!estimate) {
            const quote = quoter.quote(REQUEST.req);
            setEstimate(quote);
            console.log(quote);
        }
        else {
            console.log(estimate);
        }
    }

    async function getDeliveryQuote() {
        if (!deliveryQuote) {
            const delQuote = await quoter.quoteDelivery(estimate.quote, TARGET_DAYS)
            setDeliveryQuote(delQuote);
            console.log(delQuote);
        }
        else {
            console.log(deliveryQuote);
        }
    }

    async function getFinalQuote() {
        if (!finalQuote) {
            const finalizedQuote = await quoter.getFinalizedQuote(REQUEST.req, TARGET_DAYS);
            setFinalQuote(finalizedQuote);
            console.log(finalizedQuote);
        }
        else {
            console.log("finalQuote: ", finalQuote);
        }
    }

    async function submitProject() {
        if (!projectReceipt) {
            const project = await quoter.submitOrder(finalQuote.quoteId, TARGET_DAYS);
            setProjectReceipt(project);
            console.log(project);
        }
        else {
            console.log("projectReceipt: ", projectReceipt);
        }
    }

    async function cancelProject() {
        if (projectReceipt) {
            await quoter.cancelProject(projectReceipt.id, "My dog clicked submit :(");
            setProjectReceipt(undefined);
            console.log('Project Cancelled');
        }
        else {
            console.log('No project to cancel');
        }
    }

    return (
      <div className="App">
        <header className="App-header">
          <button onClick={init}>
              Init
          </button>
          <button onClick={getEstimate}>
              Get Estimate
          </button>
            <button onClick={getDeliveryQuote}>
                Get Delivery Quote
            </button>
            <button onClick={getFinalQuote}>
                Get Final Quote
            </button>
            <button onClick={submitProject}>
                Submit Project
            </button>
            <button onClick={ async () => {
                let status = await quoter.getProjectStatus(projectReceipt.id)
                console.log("project status: ", status)
            }}>
                Get Status
            </button>
            <button onClick={cancelProject}>
                Cancel Project
            </button>
            <button onClick={ () => console.log("available powders: ", quoter.getAvailablePowders())}>
                Print Powders
            </button>
            <button onClick={ () => console.log("available powders: ", quoter.getConfig())}>
                Print Config
            </button>
        </header>
      </div>
    );
}

export default App;
